With over 20 years of experience we provide much more than just hearing aids. We customize our hearing solutions around each individuals wants needs and goals. We all lead different lives and have our own unique challenges.

Address: 22725 44th Ave W, #202, Mountlake Terrace, WA 98043, USA

Phone: 425-245-8507